function cbar=calCompatibleMatRow(pot1,pot2, options)
nodesum=options(1);
nodestart=options(2);
freenodestart=options(3);
freenodesum=options(4);

cbar=zeros([1,2*nodesum]);
cbar(2*nodestart-1)=pot1(1)-pot2(1);
cbar(2*nodestart)=pot1(2)-pot2(2);
cbar(2*nodestart+1)=-(pot1(1)-pot2(1));
cbar(2*nodestart+2)=-(pot1(2)-pot2(2));

cbar=1/norm(pot1-pot2)*cbar([2*freenodestart-1:2*freenodestart+2*freenodesum-2]);

end