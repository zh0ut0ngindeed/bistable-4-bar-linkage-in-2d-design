function pl=simuMotion(frame,designPara)

lenASa1=designPara(1);
lenCSa3=designPara(2);
lenBSa2a1=designPara(3);
aglBSa2a1=designPara(4);
lenBSa2a2=designPara(5);
aglBSa2a2=designPara(6);
lenOriSa1Sa2=designPara(7);
lenOriSa2Sa3=designPara(8);
kSpringSa1Sa2=designPara(9);
kSpringSa2Sa3=designPara(10);

aglAList=[];
aglDList=[];
lenSa1Sa2List=[];
lenSa2Sa3List=[];
energyList=[];

for aglAi=0:0.05:2*pi
    aglDi=calAlphaD(frame,aglAi);
    
    potAi=calPotA(frame,aglAi,aglDi);
    potBi=calPotB(frame,aglAi,aglDi);
    potCi=calPotC(frame,aglAi,aglDi);
    potDi=calPotD(frame,aglAi,aglDi);
    
    frameCoi=[potAi,potBi,potCi,potDi];
    
    [potSa1i,potSa2a1i,potSa2a2i,potSa3i]=defSlaveLoc(designPara,frameCoi);
    
    energyi=1/2*kSpringSa1Sa2*(norm(potSa1i-potSa2a1i)-lenOriSa1Sa2)^2+...
        1/2*kSpringSa2Sa3*(norm(potSa3i-potSa2a2i)-lenOriSa2Sa3)^2;
    
    aglAList=cat(2,aglAList,aglAi);
    aglDList=cat(2,aglDList,aglDi);
    lenSa1Sa2List=cat(2,lenSa1Sa2List,norm(potSa1i-potSa2a1i));
    lenSa2Sa3List=cat(2,lenSa2Sa3List,norm(potSa3i-potSa2a2i));
    energyList=cat(2,energyList,energyi);
    
end

pl="Here is the energy variation";

plot(aglAList,energyList,"m")

% hold on
% plot(aglAList,aglDList,"b")
% 
% legend('ENERGY','AngleD')



end
