function pot=calPotC(frame,alphaA,alphaD)
cd=frame(3);
da=frame(4);

pot=[da+cd*cos(alphaD);cd*sin(alphaD)];
end