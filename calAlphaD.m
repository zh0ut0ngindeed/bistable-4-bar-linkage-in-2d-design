function alphaD=calAlphaD(frame,alphaA)
ab=frame(1);
bc=frame(2);
cd=frame(3);
da=frame(4);

function obj=objfunc(alphaD)
obj=abs(norm(calPotA(frame,alphaA,alphaD)-calPotB(frame,alphaA,alphaD))-ab);
end

function [c,ceq]=confuncs(alphaD)

ceq1=abs(norm(calPotA(frame,alphaA,alphaD)-calPotB(frame,alphaA,alphaD))-ab);
ceq2=abs(norm(calPotB(frame,alphaA,alphaD)-calPotC(frame,alphaA,alphaD))-bc);
ceq3=abs(norm(calPotC(frame,alphaA,alphaD)-calPotD(frame,alphaA,alphaD))-cd);

ceq=[ceq1;ceq2;ceq3];
c=[];
end

obj=@objfunc;
A = 1;
b = pi;
Aeq = [];
beq = [];
lb = [];
ub = [];
nonlcon = @confuncs;
x0 = pi/2;
alphaD=fmincon(obj,x0,A,b,Aeq,beq,lb,ub,nonlcon);

end