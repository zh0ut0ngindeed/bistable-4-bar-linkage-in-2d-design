function [dm,dc]=calMainDisplacement(frame,alphaA)

ab=frame(1);
bc=frame(2);
cd=frame(3);
da=frame(4);

hMatHat2=eye(2);
hMatHat4=[1, 0, -1, 0; 0, 1, 0, -1;-1, 0, 1, 0;0, -1, 0, 1];

alphaD=calAlphaD(frame,alphaA);

potA=calPotA(frame,alphaA,alphaD);
potB=calPotB(frame,alphaA,alphaD);
potC=calPotC(frame,alphaA,alphaD);
potD=calPotD(frame,alphaA,alphaD);

cBar=[
    calCompatibleMatRow(potA,potB,[4,1,2,2]);
    calCompatibleMatRow(potB,potC,[4,2,2,2]);
    calCompatibleMatRow(potC,potD,[4,3,2,2])
    ];

wM=null(cBar);
cBarPseInv=pinv(cBar);

extOrd2=[
    1/2/ab*wM(1:2)'*hMatHat2*wM(1:2);
    1/2/bc*wM'*hMatHat4*wM;
    1/2/cd*wM(3:4)'*hMatHat2*wM(3:4)];

dm=wM;
dc=-cBarPseInv*extOrd2;

end






