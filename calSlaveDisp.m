function [dmSa1,dcSa1,dmSa2a1,dcSa2a1,dmSa2a2,dcSa2a2,dmSa3,dcSa3]=calSlaveDisp(frame,alphaA,designPara,frameCoordinate)
ab=frame(1);
bc=frame(2);
cd=frame(3);
da=frame(4);

lenASa1=designPara(1);
lenCSa3=designPara(2);
lenBSa2a1=designPara(3);
aglBSa2a1=designPara(4);
lenBSa2a2=designPara(5);
aglBSa2a2=designPara(6);

potA=frameCoordinate(:,1);
potB=frameCoordinate(:,2);
potC=frameCoordinate(:,3);
potD=frameCoordinate(:,4);

[dm,dc]=calMainDisplacement(frame,alphaA);
[potSa1,potSa2a1,potSa2a2,potSa3]=defSlaveLoc(designPara,frameCoordinate);

% Displacement at the 1st order
dmSa1=lenASa1/ab*dm(1:2);
dmSa3=(cd - lenCSa3)/cd*dm(3:4);
dmSa2a1=inv([potB'-potSa2a1';potC'-potSa2a1'])*[(potB'-potSa2a1')*dm(1:2);(potC'-potSa2a1')*dm(3:4)];
dmSa2a2=inv([potB'-potSa2a2';potC'-potSa2a2'])*[(potB'-potSa2a2')*dm(1:2);(potC'-potSa2a2')*dm(3:4)];

% Displacement at the 2nd order
dcSa1=lenASa1/ab*dc(1:2);
dcSa3=(cd - lenCSa3)/cd*dc(3:4);
dcSa2a1=inv([potB'-potSa2a1';potC'-potSa2a1'])*([(potB'-potSa2a1')*dc(1:2);(potC'-potSa2a1')*dc(3:4)]+[1/2*(dm(1:2)'-dmSa2a1')*(dm(1:2)-dmSa2a1);1/2*(dm(3:4)'-dmSa2a1')*(dm(3:4)-dmSa2a1)]);
dcSa2a2=inv([potB'-potSa2a2';potC'-potSa2a2'])*([(potB'-potSa2a2')*dc(1:2);(potC'-potSa2a2')*dc(3:4)]+[1/2*(dm(1:2)'-dmSa2a2')*(dm(1:2)-dmSa2a2);1/2*(dm(3:4)'-dmSa2a2')*(dm(3:4)-dmSa2a2)]);

end



