%{
"Handbook of Applying Energy Landscape Method on 4-bar Structure"
Version: 3.2.1
Previous Version: file on Week19
Created Date: 2021-11-06
Last update Date:2021-11-06
Content: Bi-stability. Slave structure is on the Sa1-Sa2 and Sa2-Sa3
Update Content: Kinematic simulation
%}

clc
clear
format long

% designPara=[lenASa1,lenCSa3,lenBSa2a1,aglBSa2a1,lenBSa2a2,aglBSa2a2,... 
%             lenOriSa1Sa2,lenOriSa2Sa3,kSpringSa1Sa2,kSpringSa2Sa3];

obj=@objfunc;
A = [];
b = [];
Aeq = [];
beq = [];
lb = [0.05, 0.05,   0.05,   0.0001, 0.1,    0.0001, 0.001,  0.001,  1,  1];
ub = [0.35, 0.75,   1.00,   1*pi,   1.00,   1*pi,   2.00,   2.00,   15, 15];
nonlcon = @confuncs;
x0 = [0.10, 0.10,   0.10,   pi/2,   0.10,   pi/2,   1.00,   1.00,   10, 10];
options=optimoptions('fmincon','ConstraintTolerance',1e-12);
designParares=fmincon(obj,x0,A,b,Aeq,beq,lb,ub,nonlcon,options)

% Review
ab = 0.4;
bc = 0.7;
cd = 0.8;
da = 1.0;
frame=[ab;bc;cd;da];

alphaAc1 = 1/6*pi; % stable-to-be C1
alphaAc2 = 3/4*pi; % stable-to-be C2
alphaDc1 = calAlphaD(frame,alphaAc1);
alphaDc2 = calAlphaD(frame,alphaAc2);

hMatHat2=eye(2);
hMatHat4=[1, 0, -1, 0; 0, 1, 0, -1;-1, 0, 1, 0;0, -1, 0, 1];

frameCoordinatec1=[
    calPotA(frame,alphaAc1,alphaDc1),calPotB(frame,alphaAc1,alphaDc1),...
    calPotC(frame,alphaAc1,alphaDc1),calPotD(frame,alphaAc1,alphaDc1)];
frameCoordinatec2=[
    calPotA(frame,alphaAc2,alphaDc2),calPotB(frame,alphaAc2,alphaDc2),...
    calPotC(frame,alphaAc2,alphaDc2),calPotD(frame,alphaAc2,alphaDc2)];

[deltaEnergyOrd1c1,deltaEnergyOrd2c1]=...
    calSlaveEnergy(frame,alphaAc1,designParares,frameCoordinatec1);
[deltaEnergyOrd1c2,deltaEnergyOrd2c2]=...
    calSlaveEnergy(frame,alphaAc2,designParares,frameCoordinatec2);

simuMotion(frame,designParares)

[potSa1C1,potSa2a1C1,potSa2a2C1,potSa3C1]=defSlaveLoc(designParares,frameCoordinatec1);
[potSa1C2,potSa2a1C2,potSa2a2C2,potSa3C2]=defSlaveLoc(designParares,frameCoordinatec2);

function obj=objfunc(designPara)

ab = 0.4;
bc = 0.7;
cd = 0.8;
da = 1.0;

frame=[ab;bc;cd;da];

alphaAc1 = 1/6*pi;
alphaAc2 = 3/4*pi;
alphaDc1 = calAlphaD(frame,alphaAc1);
alphaDc2 = calAlphaD(frame,alphaAc2);

frameCoordinatec1=[
    calPotA(frame,alphaAc1,alphaDc1),calPotB(frame,alphaAc1,alphaDc1),...
    calPotC(frame,alphaAc1,alphaDc1),calPotD(frame,alphaAc1,alphaDc1)];
frameCoordinatec2=[
    calPotA(frame,alphaAc2,alphaDc2),calPotB(frame,alphaAc2,alphaDc2),...
    calPotC(frame,alphaAc2,alphaDc2),calPotD(frame,alphaAc2,alphaDc2)];


[deltaEnergyOrd1c1,deltaEnergyOrd2c1]=...
    calSlaveEnergy(frame,alphaAc1,designPara,frameCoordinatec1);
[deltaEnergyOrd1c2,deltaEnergyOrd2c2]=...
    calSlaveEnergy(frame,alphaAc2,designPara,frameCoordinatec2);

obj=-min(deltaEnergyOrd2c1,deltaEnergyOrd2c2);
end

function [c,ceq]=confuncs(designPara)

ab = 0.4;
bc = 0.7;
cd = 0.8;
da = 1.0;

frame=[ab;bc;cd;da];

alphaAc1 = 1/6*pi;
alphaAc2 = 3/4*pi;
alphaDc1 = calAlphaD(frame,alphaAc1);
alphaDc2 = calAlphaD(frame,alphaAc2);

frameCoordinatec1=[
    calPotA(frame,alphaAc1,alphaDc1),calPotB(frame,alphaAc1,alphaDc1),...
    calPotC(frame,alphaAc1,alphaDc1),calPotD(frame,alphaAc1,alphaDc1)];
frameCoordinatec2=[
    calPotA(frame,alphaAc2,alphaDc2),calPotB(frame,alphaAc2,alphaDc2),...
    calPotC(frame,alphaAc2,alphaDc2),calPotD(frame,alphaAc2,alphaDc2)];

[deltaEnergyOrd1c1,deltaEnergyOrd2c1]=calSlaveEnergy(frame,alphaAc1,designPara,frameCoordinatec1);
[deltaEnergyOrd1c2,deltaEnergyOrd2c2]=calSlaveEnergy(frame,alphaAc2,designPara,frameCoordinatec2);

ceq1=deltaEnergyOrd1c1;
ceq2=deltaEnergyOrd1c2;

c1=-deltaEnergyOrd2c1;
c2=-deltaEnergyOrd2c2;
c3=-deltaEnergyOrd2c1;
c4=-deltaEnergyOrd2c2;

ceq=[ceq1;ceq2];
c=[c1;c2;c3;c4];
end













