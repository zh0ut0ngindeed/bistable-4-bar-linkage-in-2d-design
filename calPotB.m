function pot=calPotB(frame,alphaA,alphaD)
ab=frame(1);

pot=[ab*cos(alphaA);ab*sin(alphaA)];

end