function [potSa1,potSa2a1,potSa2a2,potSa3]=defSlaveLoc(designPara,frameCoordinate)

lenASa1=designPara(1);
lenCSa3=designPara(2);
lenBSa2a1=designPara(3);
aglBSa2a1=designPara(4);
lenBSa2a2=designPara(5);
aglBSa2a2=designPara(6);

potA=frameCoordinate(:,1);
potB=frameCoordinate(:,2);
potC=frameCoordinate(:,3);
potD=frameCoordinate(:,4);

% Point Sa1 and Sa3
potSa1=lenASa1/norm(potA-potB)*(potB - potA) + potA;
potSa3=lenCSa3/norm(potC-potD)*(potD - potC) + potC;

% Point Sa2
dircflag=cross([1;0;0],[potC-potB;0]);
if dircflag(3)>=0
    rotationAngle=acos((potC-potB)'*[1;0]/norm(potC-potB));
    rotationMat=[cos(rotationAngle),-sin(rotationAngle);sin(rotationAngle),cos(rotationAngle)];
    potSa2a1=potB+rotationMat*[lenBSa2a1*cos(aglBSa2a1);lenBSa2a1*sin(aglBSa2a1)];
else
    rotationAngle=-acos((potC-potB)'*[1;0]/norm(potC-potB));
    rotationMat=[cos(rotationAngle),-sin(rotationAngle);sin(rotationAngle),cos(rotationAngle)];
    potSa2a1=potB+rotationMat*[lenBSa2a1*cos(aglBSa2a1);lenBSa2a1*sin(aglBSa2a1)];
end
dircflag=cross([1;0;0],[potC-potB;0]);
if dircflag(3)>=0
    rotationAngle=acos((potC-potB)'*[1;0]/norm(potC-potB));
    rotationMat=[cos(rotationAngle),-sin(rotationAngle);sin(rotationAngle),cos(rotationAngle)];
    potSa2a2=potB+rotationMat*[lenBSa2a2*cos(aglBSa2a2);lenBSa2a2*sin(aglBSa2a2)];
else
    rotationAngle=-acos((potC-potB)'*[1;0]/norm(potC-potB));
    rotationMat=[cos(rotationAngle),-sin(rotationAngle);sin(rotationAngle),cos(rotationAngle)];
    potSa2a2=potB+rotationMat*[lenBSa2a2*cos(aglBSa2a2);lenBSa2a2*sin(aglBSa2a2)];
end

end