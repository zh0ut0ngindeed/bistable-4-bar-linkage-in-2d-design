function [deltaEnergyOrd1,deltaEnergyOrd2]=calSlaveEnergy(frame,alphaA,designPara,frameCoordinate)

hMatHat2=eye(2);
hMatHat4=[1, 0, -1, 0; 0, 1, 0, -1;-1, 0, 1, 0; 0, -1, 0, 1];

lenASa1=designPara(1);
lenCSa3=designPara(2);
lenBSa2a1=designPara(3);
aglBSa2a1=designPara(4);
lenBSa2a2=designPara(5);
aglBSa2a2=designPara(6);
lenOriSa1Sa2=designPara(7);
lenOriSa2Sa3=designPara(8);
kSpringSa1Sa2=designPara(9);
kSpringSa2Sa3=designPara(10);


[potSa1,potSa2a1,potSa2a2,potSa3]=defSlaveLoc(designPara,frameCoordinate);
[dmSa1,dcSa1,dmSa2a1,dcSa2a1,dmSa2a2,dcSa2a2,dmSa3,dcSa3]=calSlaveDisp(frame,alphaA,designPara,frameCoordinate);

cSpringSa1Sa2=calCompatibleMatRow(potSa1,potSa2a1, [2, 1, 1, 2]);
cSpringSa2Sa3=calCompatibleMatRow(potSa2a2,potSa3, [2, 1, 1, 2]);

extSa1Sa2Ord1=cSpringSa1Sa2*[dmSa1;dmSa2a1];
extSa2Sa3Ord1=cSpringSa2Sa3*[dmSa2a2;dmSa3];
extSa1Sa2Ord2=cSpringSa1Sa2*[dcSa1;dcSa2a1]+1/2/norm(potSa1-potSa2a1)*[dmSa1;dmSa2a1]'*(hMatHat4-cSpringSa1Sa2'*cSpringSa1Sa2)*[dmSa1;dmSa2a1];
extSa2Sa3Ord2=cSpringSa2Sa3*[dcSa2a2;dcSa3]+1/2/norm(potSa2a2-potSa3)*[dmSa2a2;dmSa3]'*(hMatHat4-cSpringSa2Sa3'*cSpringSa2Sa3)*[dmSa2a2;dmSa3];

deltaEnergyOrd1=(norm(potSa1-potSa2a1)-lenOriSa1Sa2)*kSpringSa1Sa2*extSa1Sa2Ord1+...
    (norm(potSa2a2-potSa3)-lenOriSa2Sa3)*kSpringSa2Sa3*extSa2Sa3Ord1;
deltaEnergyOrd2=(norm(potSa1-potSa2a1)-lenOriSa1Sa2)*kSpringSa1Sa2*extSa1Sa2Ord2+...
    1/2*kSpringSa1Sa2*extSa1Sa2Ord1^2+...
    (norm(potSa2a2-potSa3)-lenOriSa2Sa3)*kSpringSa2Sa3*extSa2Sa3Ord2+...
    1/2*kSpringSa2Sa3*extSa2Sa3Ord1^2;

end






